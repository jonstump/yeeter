import React from "react";
import PropTypes from "prop-types";
import avatarImage from "./../img/avatar.png"

function Post(props){
  return (
    <>
      <img src={avatarImage} alt="test" />
      <p>Username: {props.user_name}</p>
      <p>Post Details: {props.post}</p>
    </>
  )
}

Post.propTypes = {
  avatar: PropTypes.string,
  user_name: PropTypes.string.isRequired,
  post: PropTypes.string.isRequired
};

export default Post;