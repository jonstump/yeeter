import React from "react"
import '../App.css';
import HomeFeed from "./HomeFeed"
import SuggestFriendsList from "./SuggestFriendsList"
import ProfileBox from "./ProfileBox"
import TopBar from "./TopBar"

function App() {
  return (
    <>
      <TopBar />
      <div class="main">
        <ProfileBox />
        <div class="friendList">
          <SuggestFriendsList />
        </div>
        <HomeFeed />
      </div>
    </>
  );
}

export default App;