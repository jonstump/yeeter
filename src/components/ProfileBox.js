import React from 'react'
import Profile from "./Profile"
import Bio from "./Bio"

function ProfileBox() {
  return (
    <>
      <div class="profileBox">
        <Profile />
        <Bio />
      </div>
    </>
  )
}

export default ProfileBox;