import React from 'react'
import PropTypes from "prop-types";
import avatarImage from "./../img/avatar.png"

function Profile(props){
  return (
    <>
      <img src={avatarImage} alt="test" />
      <p>Username: {props.user_name}</p>
      <p>YEETS: {props.yeets}</p>
      <p>Following {props.following}</p>
      <p>Followers {props.followers}</p>
    </>
  )
}

Profile.propTypes = {
  avatar: PropTypes.string,
  user_name: PropTypes.string.isRequired,
  yeets: PropTypes.number,
  following: PropTypes.number,
  followers: PropTypes.number,
}

export default Profile;