import React from 'react'
import Friend from "./Friend"

const masterFriendsList = [
  {
    avatar: "image location",
    user_name: "Namey McNameFace"
    // button: "Accept"
  },
  {
    avatar: "image location",
    user_name: "Namey McNameFace"
  },
  {
    avatar: "image location",
    user_name: "Namey McNameFace"
  },
]

const FriendsList = () => {
  return (
    <>
      <hr/>
      {masterFriendsList.map((friend, index) =>
      <Friend avatar={friend.avatar}
      user_name={friend.user_name}/>,
      // <button key={index} name="Accept">
      //   Accept
      // </button>
      )}
    </>
  )
};

export default FriendsList;