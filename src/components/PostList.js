import React from "react";
import Post from "./Post.js"

const masterPostList = [
  {
    avatar: "image location",
    user_name: "Name McNamerson",
    post: "Here's my post of me screaming into the void that is social media."
  },
  {
    avatar: "image location",
    user_name: "Name McNamerson",
    post: "Here's my post of me screaming into the void that is social media."
  },
  {
    avatar: "image location",
    user_name: "Name McNamerson",
    post: "Here's my post of me screaming into the void that is social media."
  },
  {
    avatar: "image location",
    user_name: "Name McNamerson",
    post: "Here's my post of me screaming into the void that is social media."
  },
];

const PostList = () => {
  return (
    <>
    <hr/>
    {masterPostList.map((post, index) =>
    <Post avatar={post.avatar}
      user_name={post.user_name}
      post={post.post}/>
      )}
    </>
  )
};

export default PostList;