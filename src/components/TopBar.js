import React from 'react'
import Home from './Home'
import Notifications from './Notifications'
import Messages from './Messages'
import Search from './Search'
import Yeet from './Yeet'

const TopBar = () => {
  return (
    <>
    <div id="TopBar">
      <div class="nav">
        <Home />
        <Notifications />
        <Messages />
      </div>
      <div class="right">
        <Search />
        <Yeet />
      </div>
    </div>
    <hr/>
    </>
  )
}

export default TopBar