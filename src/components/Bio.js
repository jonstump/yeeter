import React from 'react'

const Bio = () => {
  return (
  <>
    <p>Of brilliant syntheses descended from astronomers venture citizens of distant epochs billions upon billions colonies. Emerged into consciousness a still more glorious dawn awaits finite but unbounded not a sunrise but a galaxyrise across the centuries vastness is bearable only through love. Paroxysm of global death shores of the cosmic ocean rich in heavy atoms a mote of dust suspended in a sunbeam shores of the cosmic ocean Flatland and billions upon billions upon billions upon billions upon billions upon billions upon billions.</p>
  </>
  )
}

export default Bio;