import React from "react"
import PostList from "./PostList"
import PostForm from "./PostForm"

function HomeFeed() {
  return (
    <>
      <div class="homeFeed">
        <PostForm />
        <PostList />
      </div>
    </>
  );
};

export default HomeFeed;