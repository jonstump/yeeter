import React from 'react'
import PropTypes from "prop-types";
import avatarImage from "./../img/avatar.png"

function Friend(props){
  return (
    <>
      <img src={avatarImage} alt="test" />
      <p>Username: {props.user_name}</p>
      <input type="submit" value="Accept"/>
      <br/>
      <br/>
    </>
  )
}

Friend.propTypes = {
  avatar: PropTypes.string,
  user_name: PropTypes.string.isRequired
}

export default Friend;